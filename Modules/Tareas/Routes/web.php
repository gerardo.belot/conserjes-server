<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    Route::resource('tareas', 'TareasController');
    Route::get('task/data-array', 'TaskDatatableController@index');
    Route::resource('clientes', 'ConsignedController');
    Route::get('cliente/data-array', 'ConsignedDataTableController@index');

    Route::resource('conserjes', 'ConserjesController');

});
Route::get('clientes-ajax', 'ConsignedRemoteController@index');
