<?php

namespace Modules\Tareas\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Tareas\Entities\Consigned;
use Yajra\DataTables\DataTables;

class ConsignedDataTableController extends Controller
{

    public function __construct()
    {
        $this->middleware(['role:administrador', 'permission:ver cliente'])->only(['index']);
    }

    /**
     * Display a listing of the resource.
     * @param DataTables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Datatables $datatables)
    {
        $builder = Consigned::query()->select('*')->with('clinic');

        return $datatables->eloquent($builder)
            ->editColumn('name', function ($consigned) {
                return '<a class="exe" href="' . route('clientes.show', $consigned->id) . '">' . $consigned->name . '</a>';
            })
            ->rawColumns(['name'])
            ->make(true);

    }
}
