<?php

namespace Modules\Tareas\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Tareas\Entities\Task;
use Yajra\DataTables\DataTables;

class TaskDatatableController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:administrador', 'permission:ver tareas'])->only(['index']);
    }

    /**
     * Display a listing of the resource.
     * @param DataTables $datatables
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function index(Datatables $datatables)
    {
        $builder = Task::with('consigned')->select(
            "tasks.id",
            "consigneds.name as consignedName",
            "users.name as conserjeName",
            "users.id as uid",
            "types.name as typesNames",
            "tasks.created_at",
            "tasks.updated_at",
            "urgents.name as urgentNames",
            "states.name as statesNames",
            "tasks.state_id",
            "tasks.urgent_id"
            )
            ->Join('consigneds', 'tasks.consigned_id', '=', 'consigneds.id')
            ->Join("types", "type_id", "=", "types.id")
            ->Join("urgents", "urgent_id", "=", "urgents.id")
            ->Join("states", "state_id", "=", "states.id")
            ->Join("users", "conserje_id", "=", "users.id");

        return $datatables->eloquent($builder)
            ->editColumn('consignedName', function ($task) {
                return '<a class="exe" href="' . route('tareas.show', $task->id) . '">' . $task->consignedName . '</a>';
            })
            ->editColumn('conserjeName', function($users){
                return '<a class="exe" href="' . route('conserjes.show', $users->uid) . '">' . $users->conserjeName . '</a>';;
            })
            ->editColumn('updated_at', function($task){
                return $task->updated_at->format('d/m/Y h:m');
            })
            ->rawColumns(['consignedName', 'conserjeName'])
            ->make(true);
    }


}
