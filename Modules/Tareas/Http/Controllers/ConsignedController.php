<?php

namespace Modules\Tareas\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Tareas\Entities\Clinic;
use Modules\Tareas\Entities\Consigned;

class ConsignedController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:administrador', 'permission:ver cliente'])->only(['index', 'show']);
        $this->middleware(['role:administrador', 'permission:crear clientes'])->only(['create', 'store']);
        $this->middleware(['role:administrador', 'permission:editar clientes'])->only(['edit', 'update']);
        $this->middleware(['role:administrador', 'permission:borrar clientes'])->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('tareas::consigned.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $clinicas = Clinic::all()->pluck('name', 'id');
        $clinicas->prepend('Select', 'Select');
        return view('tareas::consigned.create', compact('clinicas'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $cliente = Consigned::create($request->all());
        return redirect()->to(route('clientes.index'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $cliente = Consigned::findOrfail($id);
        return view('tareas::consigned.show', compact('cliente'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $cliente = Consigned::findOrFail($id);
        $clinicas = Clinic::all()->pluck('name', 'id');
        $clinicas->prepend('Select', 'Select');

        return view('tareas::consigned.edit', compact('cliente', 'clinicas'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $cliente = Consigned::findOrFail($id);
        $cliente->update($request->all());

        return redirect()->to(route('clientes.index'));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
