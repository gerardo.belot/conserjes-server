<?php

namespace Modules\Tareas\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Tareas\Entities\Consigned;

class ConsignedRemoteController extends Controller
{

    public function index()
    {
        $search = \request('q');

        $clientes = Consigned::where('name', 'LIKE', '%' . $search . '%')->select('id', 'name as text')->limit(5)->get();
        $results['results'] = $clientes;

        return \Response::json($results);
    }

}
