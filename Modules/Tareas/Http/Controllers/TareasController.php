<?php

namespace Modules\Tareas\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Tareas\Entities\Consigned;
use Modules\Tareas\Entities\Task;
use Modules\Tareas\Entities\Type;
use Modules\Tareas\Entities\Urgent;

class TareasController extends Controller
{

    public function __construct()
    {
        $this->middleware(['role:administrador', 'permission:ver tareas'])->only(['show', 'index']);
        $this->middleware(['role:administrador', 'permission:crear tareas'])->only(['create', 'store']);
        $this->middleware(['role:administrador', 'permission:editar tareas'])->only(['edit', 'update']);
        $this->middleware(['role:administrador', 'permission:borrar tareas'])->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('tareas::tareas.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $consigned = Consigned::query()->limit(5)->pluck('name', 'id');
        $type = Type::all()->pluck('name', 'id');
        $users = User::with('roles')->get();
        $urgencias = Urgent::all()->pluck('name', 'id');

        $conserjes = $users->reject(function ($user, $key) {
            return $user->hasRole(['administrador', 'recepcionista']);
        })->pluck('name', 'id');

        return view('tareas::tareas.create', compact('consigned', 'type', 'conserjes', 'urgencias'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'consigned_id' => 'required',
            'type_id' => 'required',
            'conserje_id' => 'required',
            'urgent_id' => 'required',
        ]);

        $request['user_id'] = Auth::user()->id;

        $task = Task::create($request->all());

        return redirect()->to(route('tareas.index'));

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $tarea = Task::findOrFail($id);
        return view('tareas::tareas.show', compact('tarea'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('tareas::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
