<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('conserje_id');
            $table->integer('user_id');
            $table->integer('consigned_id');
            $table->integer('type_id');
            $table->integer('state_id')->default(1);
            $table->integer('urgent_id');
            $table->text('notes')->nullable();
            $table->decimal('lat', 10, 8);
            $table->decimal('long', 10, 8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
