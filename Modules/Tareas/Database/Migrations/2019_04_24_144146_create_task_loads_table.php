<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskLoadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_loads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('task_id')->index();
            $table->integer('consigned_id')->index()->nullable();
            $table->integer('load_id')->index()->nullable();
            $table->integer('examenes')->nullable();
            $table->integer('muestras')->nullable();
            $table->string('address')->nullable();
            $table->integer('paquetes')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_loads');
    }
}
