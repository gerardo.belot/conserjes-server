<?php

use Faker\Generator as Faker;
use Modules\Tareas\Entities\Load;

$factory->define(Load::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
    ];
});
