<?php

use Faker\Generator as Faker;
use Modules\Tareas\Entities\Type;

$factory->define(Type::class, function (Faker $faker) {
    return [
        'name' => $faker->word
    ];
});
