<?php

use Faker\Generator as Faker;

function getInstanceOf($class, $returnIdOnly = true)
{
    $instance = $class::inRandomOrder()->first() ?? factory($class)->create();
    return $returnIdOnly ? $instance->id : $instance;
}

$factory->define(Modules\Tareas\Entities\Consigned::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'address' => $faker->address,
        'clinic_id' => getInstanceOf(Modules\Tareas\Entities\Clinic::class),
    ];
});
