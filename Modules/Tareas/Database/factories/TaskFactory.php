<?php

use App\User;
use Faker\Generator as Faker;
use Modules\Tareas\Entities\Consigned;
use Modules\Tareas\Entities\State;
use Modules\Tareas\Entities\Task;
use Modules\Tareas\Entities\Type;
use Modules\Tareas\Entities\Urgent;


function getInstanceOfUsers($class, $returnIdOnly = true)
{
    $instance = $class::inRandomOrder()->first() ?? factory($class)->create();
    return $returnIdOnly ? $instance->id : $instance;
}

$factory->define(Task::class, function (Faker $faker) {
    return [
        'user_id' => getInstanceOfUsers(User::class),
        'conserje_id' => getInstanceOfUsers(User::class),
        'consigned_id' => getInstanceOfUsers(Consigned::class),
        'type_id' => getInstanceOfUsers(Type::class),
        'state_id' => getInstanceOfUsers(State::class),
        'urgent_id' => getInstanceOfUsers(Urgent::class),
        'notes' => $faker->sentence,
    ];
});
