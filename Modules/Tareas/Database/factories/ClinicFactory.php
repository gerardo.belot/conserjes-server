<?php

use Faker\Generator as Faker;
use Modules\Tareas\Entities\Clinic;

$factory->define(Clinic::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'address' => $faker->address
    ];
});
