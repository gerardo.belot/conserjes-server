<?php

namespace Modules\Tareas\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Tareas\Entities\Clinic;
use Modules\Tareas\Entities\Consigned;
use Modules\Tareas\Entities\Load;
use Modules\Tareas\Entities\Task;
use Modules\Tareas\Entities\Type;
use Nwidart\Modules\Module;

class TareasDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        factory(Clinic::class, 20)->create();
        $this->call(TypeTableSeeder::class);
        $this->call(StateTableSeeder::class);
        $this->call(UrgentTableSeeder::class);
        factory(Load::class, 10)->create();
        factory(Consigned::class, 20)->create();
        factory(Task::class, 20)->create();
    }
}
