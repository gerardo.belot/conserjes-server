<?php

namespace Modules\Tareas\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->delete();

        DB::table('types')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'name' => 'Clinica Interna',
                    'created_at' => '2019-04-21 17:05:49',
                    'updated_at' => '2019-04-21 17:05:49',
                ),
            1 =>
                array (
                    'id' => 2,
                    'name' => 'Transportes Externos',
                    'created_at' => '2019-04-21 17:05:49',
                    'updated_at' => '2019-04-21 17:05:49',
                ),
            2 =>
                array (
                    'id' => 3,
                    'name' => 'Clinicas Externas',
                    'created_at' => '2019-04-21 17:05:49',
                    'updated_at' => '2019-04-21 17:05:49',
                ),
            3 =>
                array (
                    'id' => 4,
                    'name' => 'Domiciliarias',
                    'created_at' => '2019-04-21 17:05:49',
                    'updated_at' => '2019-04-21 17:05:49',
                ),
        ));

    }
}
