<?php

namespace Modules\Tareas\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->delete();

        DB::table('states')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'name' => 'Iniciado',
                    'created_at' => '2019-04-21 17:05:49',
                    'updated_at' => '2019-04-21 17:05:49',
                ),
            1 =>
                array (
                    'id' => 2,
                    'name' => 'En Transito',
                    'created_at' => '2019-04-21 17:05:49',
                    'updated_at' => '2019-04-21 17:05:49',
                ),
            2 =>
                array (
                    'id' => 3,
                    'name' => 'Finalizado',
                    'created_at' => '2019-04-21 17:05:49',
                    'updated_at' => '2019-04-21 17:05:49',
                )
        ));
    }
}
