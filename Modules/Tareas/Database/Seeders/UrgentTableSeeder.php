<?php

namespace Modules\Tareas\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UrgentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('urgents')->delete();

        DB::table('urgents')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'name' => 'Standar',
                    'created_at' => '2019-04-21 17:05:49',
                    'updated_at' => '2019-04-21 17:05:49',
                ),
            1 =>
                array (
                    'id' => 2,
                    'name' => 'Urgente',
                    'created_at' => '2019-04-21 17:05:49',
                    'updated_at' => '2019-04-21 17:05:49',
                ),
            2 =>
                array (
                    'id' => 3,
                    'name' => 'Inmediato',
                    'created_at' => '2019-04-21 17:05:49',
                    'updated_at' => '2019-04-21 17:05:49',
                )
        ));
    }
}
