@extends('tareas::layouts.admin')

@section('content')
    @component('layouts.admin._cFrame')
        @slot('title')
            {{ $conserje->name }}
        @endslot

        @slot('buttons')
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
               role="button"
               aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    {{--<a href="{{ route('tareas.create') }}" class="btn" alt="Editar">
                        <i class="fas fa-pen"></i> Editar</a>--}}
                    <a class="btn " href="{{ route('tareas.index') }}" alt="regresar">
                        <i class="fas fa-arrow-left"></i> Regresar</a>
                </li>
            </ul>
        @endslot

        @slot('content')
            <table class="table table-bordered table-hubbered">
                <thead>
                <tr>
                    <th>Cliente</th>
                    <th>Tipo</th>
                    <th>Urgencia</th>
                    <th>Estado</th>
                </tr>
                </thead>
                <tbody>
                @foreach($query as $query)
                   <tr>
                       <td>{{ $query->consigned->name  }}</td>
                       <td>{{ $query->type->name }}</td>
                       <td>{{ $query->urgent->name }}</td>
                       <td>{{ $query->state->name }}</td>
                   </tr>

                @endforeach
                </tbody>
            </table>
        @endslot
    @endcomponent
@endsection
