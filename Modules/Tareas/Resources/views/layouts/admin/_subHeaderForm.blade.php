<div class="page-title row">
    <div class="col-sm-6 col-12 text-left">
        <h3>Sesión administrativa:
            <small>{{ Auth::user()->name }}</small>
        </h3>
    </div>

    <div class="col-sm-6 col-12 text-right">
        <div class="row">
            <div
                class="offset-xl-7 col-xl-5 col-lg-12 col-md-12 col-sm-5 col-12 form-group pull-right top_search mt-3">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                                                    <button class="btn btn-light" type="button">Go!</button>
                                                </span>
                </div>

            </div>
        </div>
    </div>
</div>
