@extends('tareas::layouts.admin')


@section('content')
    @component('layouts.admin._cFrame')
        @slot('title')
            Listado de Clientes
        @endslot
        @slot('buttons')
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
               role="button"
               aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('clientes.create') }}" class="btn" alt="Crear Cliente">
                        <i class="fa fa-plus"></i> Crear Cliente</a>
                </li>
            </ul>
        @endslot
        @slot('content')
            <table id="clientesDatatable" class="table">
                <thead>
                <th>Nombre</th>
                <th>Dirección</th>
                <th>Clinica externa</th>
                </thead>
            </table>
        @endslot
    @endcomponent
@stop
