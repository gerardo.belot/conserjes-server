@extends('tareas::layouts.admin')

@section('content')
    @component('layouts.admin._cFrame')
        @slot('title')
            {{ $cliente->name }}
        @endslot
        @slot('buttons')
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
               role="button"
               aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('clientes.index') }}" class="btn" alt="regresar">
                        <i class="fa fa-arrow-left"></i> Regresar</a>
                </li>
            </ul>
        @endslot
        @slot('content')
            <table class="table">
                <tr class="alert-info">
                    <th>Nombre</th>
                    <th>Clinica</th>
                </tr>
                <tr>
                    <td><a class="btn btn-info"
                           href="{{ route('clientes.edit', $cliente->id) }}">{{ $cliente->name }}</a></td>
                    <td>{{ isset($cliente->clinic->name) ? $cliente->clinic->name : '' }}</td>
                </tr>
                <tr class="alert-info">
                    <th colspan="2">Dirección</th>
                </tr>
                <tr>
                    <td colspan="2">{{ $cliente->address }}</td>
                </tr>
            </table>
        @endslot
    @endcomponent
@stop
