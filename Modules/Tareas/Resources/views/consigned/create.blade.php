@extends('tareas::layouts.admin')

@section('content')

    @component('layouts.admin._cFrame')
        @slot('title')
            Creación de Clientes
        @endslot
        @slot('buttons')
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
               role="button"
               aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('clientes.index') }}" class="btn" alt="regresar">
                        <i class="fa fa-arrow-left"></i> Regresar</a>
                </li>
            </ul>
        @endslot
        @slot('content')
            {!! Form::open(['route' => ['clientes.store']]) !!}
            @include('tareas::consigned._form')
            {!! Form::close() !!}
        @endslot
    @endcomponent
@stop
