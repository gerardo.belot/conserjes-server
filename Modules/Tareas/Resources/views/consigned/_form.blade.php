<div class="form-group">
    <label for="name">Nombre</label>
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="address">Dirección (Opcional)</label>
    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    <label for="address">Clinica (Opcional)</label>
    {!! Form::select('clinic_id', $clinicas, null, ['class' => 'form-control address-select']) !!}
</div>

<hr/>
<button type="submit" class="btn btn-primary">Enviar</button>
