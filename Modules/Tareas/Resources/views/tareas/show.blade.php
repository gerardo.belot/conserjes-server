@extends('tareas::layouts.admin')


@section('content')
    @component('layouts.admin._cFrame')
        @slot('title')
            Detalles de registro {{ $tarea->id }}
        @endslot
        @slot('buttons')
            @can('editar tareas')
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                   role="button"
                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="{{ route('tareas.create') }}" class="btn" alt="Editar">
                            <i class="fas fa-pen"></i> Editar</a>
                        <a class="btn " href="{{ route('tareas.index') }}" alt="regresar">
                            <i class="fas fa-arrow-left"></i> Regresar</a>
                    </li>
                </ul>
            @endcan
        @endslot
        @slot('content')
            <table class="table table-bordered">
                <thead class="alert-info">
                <th>Cliente:</th>
                <th>Fecha de Inicio</th>
                <th>Ultima actualización</th>
                <td>Captado Por:</td>
                </thead>
                <tr>
                    <td>{{ $tarea->consigned->name }}</td>
                    <th>{{ $tarea->created_at->format('d/m h:m') }}</th>
                    <td><b>{{ $tarea->state->name }}</b> a las {{ $tarea->updated_at->format('h:m') }}</td>
                    <th>{{ $tarea->user->name }}</th>
                </tr>
                <tr class="alert-info">
                    <th>Dirección del Cliente</th>
                    <th>Urgencia del Paquete</th>
                    <th>Conserje Asignado</th>
                    <th>Latencia</th>
                </tr>
                <tr>
                    <td>{{ $tarea->consigned->address }}</td>
                    <td>{{ $tarea->urgent->name }}</td>
                    <td>{{ $tarea->conserje->name }}</td>
                    <td>{{ $tarea->updated_at->diffForHumans() }}</td>
                </tr>
                <tr class="alert-info">
                    <td colspan="4">Notas:</td>
                </tr>
                <tr>
                    <td colspan="4">{{ $tarea->notes }}</td>
                </tr>
            </table>

            <table class="table-bordered table ">
                <tr>
                    <th>Carga</th>
                </tr>
                <tbody>
                @foreach($tarea->tasksLoad as $tareas)
                    <tr>
                        {{ $tareas }}
                    </tr>
                @endforeach
                </tbody>

            </table>
        @endslot
    @endcomponent
@endsection
