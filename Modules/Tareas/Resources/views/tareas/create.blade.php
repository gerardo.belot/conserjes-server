@extends('tareas::layouts.admin')

@section('content')
    @component('layouts.admin._cFrame')
        @slot('title')
            Creación de Clientes
        @endslot
        @slot('buttons')
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
               role="button"
               aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('tareas.index') }}" class="btn" alt="regresar">
                        <i class="fa fa-arrow-left"></i> Regresar</a>
                </li>
            </ul>
        @endslot
        @slot('content')
            {!! Form::open(['route' => 'tareas.store', 'method' => 'POST']) !!}
            <div class="form-group">
                <label>De Cliente:</label>
                {!! Form::select('consigned_id', $consigned, null,  ['class' => 'form-control clients-select']) !!}
            </div>

            <div class="form-group">
                <label>Asignar a Conserjes:</label>
                {!! Form::select('conserje_id', $conserjes, null,  ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                <label>De Tipo:</label>
                {!! Form::select('type_id', $type, null,  ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                <label>Urgencia:</label>
                {!! Form::select('urgent_id', $urgencias, null,  ['class' => 'form-control']) !!}
            </div>


            <div class="form-group">
                <label>Notas: </label>
                {!! Form::textarea('notes', null,  ['class' => 'form-control']) !!}
            </div>


            <hr/>
            <button type="submit" class="btn btn-primary">Crear Tarea</button>
            {!!  Form::close() !!}
        @endslot
    @endcomponent
@stop
