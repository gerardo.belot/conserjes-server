@extends('tareas::layouts.admin')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ action('HomeController@index') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Administración de Tareas</li>
        </ol>
    </nav>
@endsection

@section('content')

    @component('layouts.admin._cFrame')
        @slot('title')
            Control de Tareas
        @endslot
        @slot('buttons')
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
               role="button"
               aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ route('tareas.create') }}" class="btn" alt="Crear tareas">
                        <i class="fa fa-plus"></i> Nueva Tarea</a>
                </li>
            </ul>
        @endslot
        @slot('content')
            <table id="dataTable" class="table">
                <thead>
                <th>No.</th>
                <th>Cliente</th>
                <th>Tipo</th>
                <th>Conserje:</th>
                <th>Actualizada a:</th>
                <th>Urgencia</th>
                <th>Estado</th>
                {{--


                --}}
                </thead>
                <tbody>
                </tbody>
            </table>
        @endslot
    @endcomponent
@stop
