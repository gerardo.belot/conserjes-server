$(document).ready(() => {
    // Instanciado:Consigned._form
    $('.address-select').select2();
    $('.select2multiple').select2({
        multiEntry: true
    });
    $('.clients-select').select2({
        ajax: {
                url: '/clientes-ajax',
                dataType: 'json',
            },
        width: '100%'
        }
    );

});
