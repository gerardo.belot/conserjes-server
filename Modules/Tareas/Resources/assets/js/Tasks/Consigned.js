$(document).ready(() => {
    $('#clientesDatatable').dataTable({
        serverSide: true,
        processing: true,
        //"order": [[0, "desc"]],
        ajax: 'cliente/data-array',
        columns: [
            {data: 'name', name: 'name'},
            {data: 'address', name: 'address'},
            {data: 'clinic.name', name: 'clinic.name', "defaultContent": "n/a"}
        ],
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                    .on('change', function () {
                        column.search($(this).val(), false, false, true).draw();
                    });
            });
        },
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Registro no encotrado - lo sentimos",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros de esa busqueda",
            "infoFiltered": "(filtrado de _MAX_ total Total de regístros)",
            "search": "Busqueda:",
            "processing": "| ===== Procesando ===== |",
            "loadingRecords": "Cargando...",

            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": ": Ordenando por columnas asendente",
                "sortDescending": ": Ordenando por columnas desendente"
            },
        }
    });

});
