$(document).ready(() => {

    let table = $('#dataTable').dataTable({
        serverSide: true,
        processing: true,
        "order": [[0, "desc"]],
        ajax: 'task/data-array',
        "createdRow": function (row, data, dataIndex) {
            console.log(data);
            if (data['state_id'] == 2) {
                $(row).addClass('alert-info');
            } else if (data['state_id'] == 3) {
                $(row).addClass('linethrough')
            } else if (data['urgent_id'] == 3) {
                $(row).addClass('alert-danger');
            } else if (data['urgent_id'] == 2) {
                $(row).addClass('alert-warning');
            }
        },

        columns: [
            {data: 'id', name: 'tasks'},
            {data: 'consignedName', name: 'consigneds.name'},
            {data: 'typesNames', name: 'types.name'},
            {data: 'conserjeName', name: 'users.name'},
            {data: 'updated_at', name: 'tasks.updated_at'},
            {data: 'urgentNames', name: 'urgents.id'},
            {data: 'statesNames', name: 'states.name'}
        ],

        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Registro no encotrado - lo sentimos",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros de esa busqueda",
            "infoFiltered": "(filtrado de _MAX_ total Total de regístros)",
            "search": "Busqueda:",
            "processing": "| ===== Procesando ===== |",
            "loadingRecords": "Cargando...",

            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": ": Ordenando por columnas asendente",
                "sortDescending": ": Ordenando por columnas desendente"
            },
        }
    });

    new $.fn.dataTable.Responsive( table, {
        details: false
    } );

});
