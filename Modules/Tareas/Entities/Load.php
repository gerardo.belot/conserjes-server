<?php

namespace Modules\Tareas\Entities;

use Illuminate\Database\Eloquent\Model;

class Load extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name'
    ];
}
