<?php

namespace Modules\Tareas\Entities;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $fillable = ['name'];
}
