<?php

namespace Modules\Tareas\Entities;

use Illuminate\Database\Eloquent\Model;

class TaskLoad extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'task_id', 'load_id', 'examenes', 'muestras'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function task()
    {
        return $this->belongsTo(Task::class, 'task_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function loads()
    {
        return $this->belongsTo(Load::class, 'load_id', 'id');
    }

}
