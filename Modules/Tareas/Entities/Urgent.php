<?php

namespace Modules\Tareas\Entities;

use Illuminate\Database\Eloquent\Model;

class Urgent extends Model
{
    protected $fillable = ['name'];
}
