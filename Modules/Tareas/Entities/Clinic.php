<?php

namespace Modules\Tareas\Entities;

use Illuminate\Database\Eloquent\Model;

class Clinic extends Model
{
    protected $fillable = [
        'name', 'address'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function consingeds()
    {
        return $this->hasMany(Consigned::class, 'clinic_id', 'id');
    }
}
