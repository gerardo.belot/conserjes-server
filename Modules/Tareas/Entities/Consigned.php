<?php

namespace Modules\Tareas\Entities;

use Illuminate\Database\Eloquent\Model;

class Consigned extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'clinic_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clinic()
    {
        return $this->belongsTo(Clinic::class, 'clinic_id', 'id');
    }
}
