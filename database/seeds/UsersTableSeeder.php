<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'administrador',
                'email' => 'gerardo.belot@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$o/coQq0O.Cn1xP2iZwK8juZPCzTOtqA4ZB8VNFs4K6Cf41FlfdKc6',
                'status' => 0,
                'remember_token' => NULL,
                'deleted_at' => NULL,
                'created_at' => '2019-04-17 20:53:43',
                'updated_at' => '2019-04-17 20:53:43',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'Belle Gusikowski DVM',
                'email' => 'alfredo90@example.net',
                'email_verified_at' => '2019-04-21 16:59:22',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'status' => 1,
                'remember_token' => 'Wx5sQX6s8c',
                'deleted_at' => NULL,
                'created_at' => '2019-04-21 16:59:22',
                'updated_at' => '2019-04-21 16:59:22',
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'Taryn Beer V',
                'email' => 'iabbott@example.org',
                'email_verified_at' => '2019-04-21 16:59:22',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'status' => 1,
                'remember_token' => 'GX2K6mmaNG',
                'deleted_at' => NULL,
                'created_at' => '2019-04-21 16:59:22',
                'updated_at' => '2019-04-21 16:59:22',
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'Josefa Raynor',
                'email' => 'zwalsh@example.com',
                'email_verified_at' => '2019-04-21 16:59:22',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'status' => 1,
                'remember_token' => 'JgyRksyBjG',
                'deleted_at' => NULL,
                'created_at' => '2019-04-21 16:59:22',
                'updated_at' => '2019-04-21 16:59:22',
            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'Wilhelmine Gerhold',
                'email' => 'yost.brycen@example.org',
                'email_verified_at' => '2019-04-21 16:59:22',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'status' => 1,
                'remember_token' => 'weCiL94Dmk',
                'deleted_at' => NULL,
                'created_at' => '2019-04-21 16:59:22',
                'updated_at' => '2019-04-21 16:59:22',
            ),
            5 =>
            array (
                'id' => 6,
                'name' => 'Dr. Cullen Jacobi DDS',
                'email' => 'juana25@example.com',
                'email_verified_at' => '2019-04-21 16:59:22',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'status' => 1,
                'remember_token' => 'C50ErBVUce',
                'deleted_at' => NULL,
                'created_at' => '2019-04-21 16:59:22',
                'updated_at' => '2019-04-21 16:59:22',
            ),
            6 =>
            array (
                'id' => 7,
                'name' => 'Mr. Webster Blanda',
                'email' => 'mallory80@example.com',
                'email_verified_at' => '2019-04-21 16:59:22',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'status' => 1,
                'remember_token' => 'JhcCXdD42V',
                'deleted_at' => NULL,
                'created_at' => '2019-04-21 16:59:22',
                'updated_at' => '2019-04-21 16:59:22',
            ),
            7 =>
            array (
                'id' => 8,
                'name' => 'Mrs. Pamela Goldner',
                'email' => 'crooks.buford@example.com',
                'email_verified_at' => '2019-04-21 16:59:22',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'status' => 1,
                'remember_token' => 'JNDfmJVxNP',
                'deleted_at' => NULL,
                'created_at' => '2019-04-21 16:59:22',
                'updated_at' => '2019-04-21 16:59:22',
            ),
            8 =>
            array (
                'id' => 9,
                'name' => 'Kylie Ferry',
                'email' => 'ppagac@example.net',
                'email_verified_at' => '2019-04-21 16:59:22',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'status' => 1,
                'remember_token' => '2FYRZY3TWA',
                'deleted_at' => NULL,
                'created_at' => '2019-04-21 16:59:22',
                'updated_at' => '2019-04-21 16:59:22',
            ),
            9 =>
            array (
                'id' => 10,
                'name' => 'Elmer Leannon',
                'email' => 'dax.jakubowski@example.net',
                'email_verified_at' => '2019-04-21 16:59:22',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'status' => 1,
                'remember_token' => 'qCBMLDAEQP',
                'deleted_at' => NULL,
                'created_at' => '2019-04-21 16:59:22',
                'updated_at' => '2019-04-21 16:59:22',
            ),
            10 =>
            array (
                'id' => 11,
                'name' => 'Markus Feest',
                'email' => 'dare.glenna@example.org',
                'email_verified_at' => '2019-04-21 16:59:22',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'status' => 1,
                'remember_token' => 'pzM211Tz9f',
                'deleted_at' => NULL,
                'created_at' => '2019-04-21 16:59:22',
                'updated_at' => '2019-04-21 16:59:22',
            ),
        ));

        $user = User::findOrFail(1);
        $user->assignRole('administrador');

        $user = User::findOrFail(2);
        $user->assignRole('recepcionista');

        $user = User::findOrFail(3);
        $user->assignRole('Conserje');

        $user = User::findOrFail(4);
        $user->assignRole('Conserje');

        $user = User::findOrFail(5);
        $user->assignRole('Conserje');

        $user = User::findOrFail(6);
        $user->assignRole('Conserje');

        $user = User::findOrFail(7);
        $user->assignRole('Conserje');

        $user = User::findOrFail(8);
        $user->assignRole('Conserje');

        $user = User::findOrFail(9);
        $user->assignRole('Conserje');

        $user = User::findOrFail(10);
        $user->assignRole('recepcionista');

        $user = User::findOrFail(11);
        $user->assignRole('recepcionista');
    }
}
