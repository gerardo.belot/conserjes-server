<?php

use Illuminate\Database\Seeder;

class TaskLoadsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('task_loads')->delete();

        \DB::table('task_loads')->insert(array (
            0 =>
            array (
                'id' => '1',
                'task_id' => '1',
                'consigned_id' => NULL,
                'load_id' => '2',
                'examenes' => '1',
                'muestras' => '11',
                'address' => NULL,
                'paquetes' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 =>
            array (
                'id' => '2',
                'task_id' => '2',
                'consigned_id' => NULL,
                'load_id' => '4',
                'examenes' => NULL,
                'muestras' => NULL,
                'address' => NULL,
                'paquetes' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 =>
            array (
                'id' => '3',
                'task_id' => '3',
                'consigned_id' => NULL,
                'load_id' => '5',
                'examenes' => '1',
                'muestras' => '1',
                'address' => NULL,
                'paquetes' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 =>
            array (
                'id' => '4',
                'task_id' => '3',
                'consigned_id' => NULL,
                'load_id' => '4',
                'examenes' => '2',
                'muestras' => NULL,
                'address' => NULL,
                'paquetes' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));


    }
}
