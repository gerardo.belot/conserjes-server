<?php

namespace Tests\Unit;

use App\Http\Controllers\RolesController;
use App\User;
use Mockery;
use Spatie\Permission\Models\Role;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RolesControllerTest extends TestCase
{
    use RefreshDatabase;

    public function index_roles_return_array_of_roles()
    {
        Role::create(['name' => 'administrador']);

        $user = factory(User::class)->create();

        $user->assignRole('administrador');

        $this->actingAs($user)
            ->withSession(['foo' => 'bar'])
            ->get("/")
            ->assertSee('Listado de Roles');

      /*  $list = $this->get('/roles');
        $list->assertSee('Listado de Roles');
        $this->assertDatabaseHas('roles', ['name' => 'administrador']);*/
    }
}
