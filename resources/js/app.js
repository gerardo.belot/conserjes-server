
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

require('../vendor/google-code-prettify/bin/prettify.min');

require('../vendor/nprogress/nprogress');

require('./custom');


require('./jquery/select2Instance');
