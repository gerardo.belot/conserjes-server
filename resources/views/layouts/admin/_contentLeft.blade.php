<button id="compose" class="btn btn-sm btn-success btn-block"
        type="button">
    COMPOSE
</button>
<a href="#">
    <div class="mail_list">
        <div class="left">
            <i class="fa fa-circle"></i> <i class="fa fa-edit"></i>
        </div>
        <div class="right">
            <h3>Dennis Mugo
                <small>3.00 PM</small>
            </h3>
            <p>Ut enim ad minim veniam, quis nostrud exercitation
                enim ad
                minim veniam, quis nostrud exercitation...</p>
        </div>
    </div>
</a>
<a href="#">
    <div class="mail_list">
        <div class="left">
            <i class="fa fa-star"></i>
        </div>
        <div class="right">
            <h3>Jane Nobert
                <small>4.09 PM</small>
            </h3>
            <p><span class="badge">To</span> Ut enim ad minim
                veniam, quis
                nostrud exercitation enim ad minim veniam, quis
                nostrud
                exercitation...</p>
        </div>
    </div>
</a>
<a href="#">
    <div class="mail_list">
        <div class="left">
            <i class="fa fa-circle-o"></i><i
                class="fa fa-paperclip"></i>
        </div>
        <div class="right">
            <h3>Musimbi Anne
                <small>4.09 PM</small>
            </h3>
            <p><span class="badge">CC</span> Ut enim ad minim
                veniam, quis
                nostrud exercitation enim ad minim veniam, quis
                nostrud
                exercitation...</p>
        </div>
    </div>
</a>
<a href="#">
    <div class="mail_list">
        <div class="left">
            <i class="fa fa-paperclip"></i>
        </div>
        <div class="right">
            <h3>Jon Dibbs
                <small>4.09 PM</small>
            </h3>
            <p>Ut enim ad minim veniam, quis nostrud exercitation
                enim ad
                minim veniam, quis nostrud exercitation...</p>
        </div>
    </div>
</a>
<a href="#">
    <div class="mail_list">
        <div class="left">
            .
        </div>
        <div class="right">
            <h3>Debbis & Raymond
                <small>4.09 PM</small>
            </h3>
            <p>Ut enim ad minim veniam, quis nostrud exercitation
                enim ad
                minim veniam, quis nostrud exercitation...</p>
        </div>
    </div>
</a>
<a href="#">
    <div class="mail_list">
        <div class="left">
            .
        </div>
        <div class="right">
            <h3>Debbis & Raymond
                <small>4.09 PM</small>
            </h3>
            <p>Ut enim ad minim veniam, quis nostrud exercitation
                enim ad
                minim veniam, quis nostrud exercitation...</p>
        </div>
    </div>
</a>
<a href="#">
    <div class="mail_list">
        <div class="left">
            <i class="fa fa-circle"></i> <i class="fa fa-edit"></i>
        </div>
        <div class="right">
            <h3>Dennis Mugo
                <small>3.00 PM</small>
            </h3>
            <p>Ut enim ad minim veniam, quis nostrud exercitation
                enim ad
                minim veniam, quis nostrud exercitation...</p>
        </div>
    </div>
</a>
<a href="#">
    <div class="mail_list">
        <div class="left">
            <i class="fa fa-star"></i>
        </div>
        <div class="right">
            <h3>Jane Nobert
                <small>4.09 PM</small>
            </h3>
            <p>Ut enim ad minim veniam, quis nostrud exercitation
                enim ad
                minim veniam, quis nostrud exercitation...</p>
        </div>
    </div>
</a>
