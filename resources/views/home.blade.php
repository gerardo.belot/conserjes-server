@extends('layouts.admin')

@section('content')

    @component('layouts.admin._cFrame')
        @slot('title')
            Dashboard
            <small></small>
        @endslot
        @slot('buttons')
        @endslot

        @slot('content')
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            You are logged in!
        @endslot
    @endcomponent
@endsection
