@extends('layouts.adminLogin')

@section('content')

    <form method="POST" action="{{ route('login') }}">
        <h1>Ingresar</h1>
        <div>
            {{ csrf_field() }}
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                   name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <div>
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                   name="password" required autocomplete="current-password">
            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        <div class="row">
            <div class="col-6 text-left">
                <button type="submit" class="btn btn-primary">
                    {{ __('Login') }}
                </button>
            </div>
            <div class="col-6 text-right">
                @if (Route::has('password.request'))
                    <a class="btn btn-secondary reset_pass" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                @endif
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="separator">
            <div class="clearfix"></div>
            <br/>

            <div>
                <h1><img src="{{ asset('images/logo.jpg') }}" /></h1>
                <p>Copyright © 2019 Laboratorios Médicos S. de R.L. </p>
            </div>
        </div>

    </form>

@endsection
