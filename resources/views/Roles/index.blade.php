@extends('layouts.admin')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ action('HomeController@index') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Roles</li>
        </ol>
    </nav>
@endsection

@section('content')
    @component('layouts.admin._cFrame')
        @slot('title')
            Listado de Roles
        @endslot
        @slot('buttons')
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
               role="button"
               aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ action('RolesController@create') }}" class="btn" alt="Crear Roles">
                        <i class="fa fa-plus"></i> Crear Rol</a>
                </li>
            </ul>
        @endslot
        @slot('content')
            <div class="list-group">
                @foreach($roles as $role)
                    <a href="{{ action('RolesController@edit', $role->id) }}"
                       class="list-group-item list-group-item-action">{{ $role->name }}</a>
                @endforeach
            </div>
        @endslot
    @endcomponent
    {{-- <div class="row justify-content-center">
         <div class="col-md-12">
             <div class="card">
                 <div class="card-header">
                     <div class="text-muted mid-derecha">
                         <a href="{{ action('RolesController@create') }}" class="btn btn-info" alt="Crear Citologia"><i class="fas fa-plus-circle"></i></a>
                     </div>

                     <h4></h4>
                 </div>

                 <div class="card-body">

                 </div>
             </div>
         </div>
     </div>--}}

@endsection
