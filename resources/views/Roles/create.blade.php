@extends('layouts.admin')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ action('RolesController@index') }}">Roles</a></li>
            <li class="breadcrumb-item active" aria-current="page">Crear Rol</li>
        </ol>
    </nav>
@endsection

@section('content')

    @component('layouts.admin._cFrame')
        @slot('title')
            Administración de Usuarios
        @endslot
        @slot('buttons')
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
               role="button"
               aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ action('RolesController@index') }}" class="btn" alt="Crear Roles">
                        <i class="fa fa-arrow-left"></i> Regresar</a>
                </li>
            </ul>
        @endslot
        @slot('content')
            <form method="POST" action="{{ action('RolesController@store') }}">
                <div class="form-group">
                    @csrf
                    <label for="nombre">Nombre</label>
                    <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" type="text" id="name"
                           value="{{ old('name') }}" name="name" required>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        @endslot
    @endcomponent


    {{--    <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Creación de Roles</h4>
                        </div>

                        <div class="card-body">

                        </div>
                    </div>
                </div>
            </div>
        </div>--}}
@endsection
