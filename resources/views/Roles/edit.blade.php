@extends('layouts.admin')
@section('content')
    @component('layouts.admin._cFrame')
        @slot('title')
            Administración de Usuarios
        @endslot
        @slot('buttons')
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
               role="button"
               aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ action('RolesController@index') }}" class="btn" alt="Crear Roles">
                        <i class="fa fa-arrow-left"></i> Regresar</a>
                </li>
            </ul>
        @endslot
        @slot('content')
            {!! Form::model($role, ['action' => ['RolesController@update', $role->id], 'method' => 'PUT']) !!}
                <div class="form-group">
                    @csrf
                    <label for="nombre">Nombre</label>
                    <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" type="text" id="name"
                           value="{{ isset($role->name) ? $role->name : ''  }}" name="name" required>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            {!! Form::close() !!}
        @endslot
    @endcomponent
@endsection
