@extends('layouts.admin')

@section('content')

    @component('layouts.admin._cFrame')
        @slot('title')
            Administración de Usuarios
        @endslot
        @slot('buttons')
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
               role="button"
               aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ action('UserController@index') }}" class="" alt="Crear usuario">
                        <i class="fa fa-arrow-left"></i> Regresar
                    </a>
                </li>
            </ul>
        @endslot
        @slot('content')
            {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]) !!}

            <div class="form-group">
                <label for="nombre">Nombre</label>
                {!! Form::text('name', null, array('class' => 'form-control ' . $errors->first('name', 'is-invalid'))) !!}
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
                @endif
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                {!! Form::text('email', null, array('class' => 'form-control ' . $errors->first('email', 'is-invalid'))) !!}
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="roles">Roles</label>
                {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control select2multiple','multiple')) !!}
            </div>
            <hr/>
            <button type="submit" class="btn btn-primary">Editar usuario</button>
            {!! Form::close() !!}

        @endslot
    @endcomponent
@endsection
