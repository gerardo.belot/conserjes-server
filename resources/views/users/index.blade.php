@extends('layouts.admin')

@section('content')

    @component('layouts.admin._cFrame')
        @slot('title')
            Administración de Usuarios
        @endslot
        @slot('buttons')
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
               role="button"
               aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ action('UserController@create') }}" class="" alt="Crear usuario">
                        <i class="fa fa-plus"></i> Crear Usario
                    </a>
                </li>
            </ul>
        @endslot
        @slot('content')
            <table class="table table-bordered table-striped">
                <thead>
                <th>Nombre</th>
                <th>email</th>
                <th>Rol</th>
                <th>Estado</th>
                </thead>
                @foreach($users as $user)
                    <tr>
                        <td><a href="{{ action('UserController@edit', $user->id) }}">{{ $user->name }}</a></td>
                        <td>{{ $user->email }}</td>
                        <td>
                            @if(!empty($user->getRoleNames()))
                                @foreach($user->getRoleNames() as $v)
                                    <label class="badge badge-success">{{ $v }}</label>
                                @endforeach
                            @endif
                        </td>
                        <td>
                            <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a>
                            <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>
                            {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            </table>
            {!! $users->render() !!}
        @endslot
    @endcomponent
    {{--    <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="text-muted mid-derecha">
                            <a href="{{ action('UserController@create') }}" class="btn btn-info" alt="Crear Usuario"><i class="fas fa-plus-circle"></i></a>
                        </div>

                        <h4>Listado de Usuarios</h4>
                    </div>

                    <div class="card-body">

                    </div>
                </div>
            </div>
        </div>--}}

@endsection
