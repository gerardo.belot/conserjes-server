@extends('layouts.admin')

@section('content')

    @component('layouts.admin._cFrame')
        @slot('title')
            Administración de Usuarios
        @endslot
        @slot('buttons')
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
               role="button"
               aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ action('UserController@index') }}" class="" alt="Crear usuario">
                        <i class="fa fa-arrow-left"></i> Regresar
                    </a>
                </li>
            </ul>
        @endslot

        @slot('content')
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <strong>Nombre:</strong>
                        {{ $user->name }}
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <strong>Email:</strong>
                        {{ $user->email }}
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <strong>Roles:</strong>
                        @if(!empty($user->getRoleNames()))
                            @foreach($user->getRoleNames() as $v)
                                <label class="badge badge-success">{{ $v }}</label>
                            @endforeach
                        @endif
                    </div>
                </div>


                @endslot

    @endcomponent
{{--
    <div class="row">
        <div class="col-md-12">

            <div class="pull-left">
                <h2>Detalles de Usuario</h2>
                <hr>
            </div>

        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <strong>Nombre:</strong>
                {{ $user->name }}
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                {{ $user->email }}
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <strong>Roles:</strong>
                @if(!empty($user->getRoleNames()))
                    @foreach($user->getRoleNames() as $v)
                        <label class="badge badge-success">{{ $v }}</label>
                    @endforeach
                @endif
            </div>
        </div>

        <div col-md-12>
            <hr/>
            <a class="btn btn-info" href="{{ action('UserController@edit', $user->id) }}">Editar Usuario</a>
        </div>

    </div>
--}}
@endsection
