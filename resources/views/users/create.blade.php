@extends('layouts.admin')

{{--@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ action('HomeController@index') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ action('UserController@index') }}">Administración de usuarios</a></li>
            <li class="breadcrumb-item active" aria-current="page">Creación de Usuarios</li>
        </ol>
    </nav>
@endsection--}}
@section('content')
    @component('layouts.admin._cFrame')
        @slot('title')
            Administración de Usuarios
        @endslot
        @slot('buttons')
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
               role="button"
               aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ action('UserController@index') }}" class="" alt="Crear usuario">
                        <i class="fa fa-arrow-left"></i> Regresar
                    </a>
                </li>
            </ul>
        @endslot
        @slot('content')
            {!! Form::open(array('route' => 'users.store','method'=>'POST')) !!}
            <div class="form-group">
                <label for="nombre">Nombre</label>
                {!! Form::text('name', null, array('class' => 'form-control ' . $errors->first('name', 'is-invalid'))) !!}
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
                @endif
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                {!! Form::text('email', null, array('class' => 'form-control ' . $errors->first('email', 'is-invalid'))) !!}
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
                @endif
            </div>

            <div class="form-group">
                <label for="password" class="">{{ __('Password') }}</label>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                       name="password" required autocomplete="new-password">
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
                @endif
            </div>

            <div class="form-group">
                <label for="password-confirm" class="">{{ __('Confirm Password') }}</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required
                       autocomplete="new-password">
            </div>

            <div class="form-group">
                <label for="roles">Roles</label>
                {!! Form::select('roles[]', $roles, null, array('class' => 'form-control select2multiple','multiple')) !!}
            </div>
            <hr/>
            <button type="submit" class="btn btn-primary">Crear Usuario</button>
            {!! Form::close() !!}

        @endslot

    @endcomponent

{{--    <div class="row">
        <div class="col-lg-12">
            <div class="pull-left">
                <h2>Editar Usuario</h2>
            </div>
            <hr/>
        </div>
    </div>


    {!! Form::open(array('route' => 'users.store','method'=>'POST')) !!}
    <div class="form-group">
        <label for="nombre">Nombre</label>
        {!! Form::text('name', null, array('class' => 'form-control ' . $errors->first('name', 'is-invalid'))) !!}
        @if ($errors->has('name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">
        <label for="email">Email</label>
        {!! Form::text('email', null, array('class' => 'form-control ' . $errors->first('email', 'is-invalid'))) !!}
        @if ($errors->has('name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">
        <label for="password" class="">{{ __('Password') }}</label>
        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
               name="password" required autocomplete="new-password">
        @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">
        <label for="password-confirm" class="">{{ __('Confirm Password') }}</label>
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required
               autocomplete="new-password">
    </div>

    <div class="form-group">
        <label for="roles">Roles</label>
        {!! Form::select('roles[]', $roles, null, array('class' => 'form-control','multiple')) !!}
    </div>
    <hr/>
    <button type="submit" class="btn btn-primary">Submit</button>
    {!! Form::close() !!}--}}


@endsection
