<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Tareas\Entities\Task;

class TaskApiController extends Controller
{
    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function tasks($id)
    {
        $query = Task::where('conserje_id', $id)
            ->whereIn('state_id', [1, 2, 3])
            ->with('consigned', 'type', 'state', 'urgent')
            ->orderBy('id', 'DESC')
            ->get();

        return response()->json($query, 200);
    }

    /**
     * @param $taskId
     * @return \Illuminate\Http\JsonResponse
     */
    public function task($taskId)
    {
        $query = Task::where('id', $taskId)
            ->with('consigned', 'type', 'state', 'urgent')
            ->first();
        return response()->json($query, 200);
    }

    /**
     * @param Request $request
     * @return Request
     */
    public function update(Request $request){
        return $request;
    }
}
