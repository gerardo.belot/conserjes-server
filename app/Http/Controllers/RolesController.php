<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{

    public function __construct()
    {
        $this->middleware(['role:administrador','permission:ver roles'])->only(['show', 'index']);
        $this->middleware(['role:administrador','permission:crear roles'])->only(['create', 'store']);
        $this->middleware(['role:administrador','permission:editar roles'])->only(['edit', 'update']);
        $this->middleware(['role:administrador','permission:suspender roles'])->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return View('roles.index', ['roles' => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique:roles',
        ]);

        if ($request->has('name')) {
            $name = $request->get('name');
        }

        Role::create(['name' => $name]);

        return redirect()->to(action('RolesController@index'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Role $role
     * @return void
     */
    public function edit(Role $role)
    {

        $perms = Permission::all()->pluck('name', 'id');
        $permissions = $role->permissions;

        foreach ($permissions as $sperm){
            $selectedTags[] = $sperm->id;
        }

        return View('roles.edit', compact('perms', 'role', 'selectedTags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique:roles,id,' .$id,
            'perms' => 'required'
        ]);

        $role = Role::findById($id);
        $role->syncPermissions($request->get('perms'));
        return redirect()->to(action('RolesController@index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
